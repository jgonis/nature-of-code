enum Events {
    CanvasResize = 0,
    StartClicked,
    StopClicked,
    ResetClicked,
    VizTypeChanged,
    UpdateEvent
}