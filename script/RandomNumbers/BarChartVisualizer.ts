///<reference path="IRandomNumberVisualizer.ts"/>

class BarChartVisualizer implements IRandomNumberVisualizer {
    
    constructor(context: CanvasRenderingContext2D) {}

    public draw(newRandomNumber: number) {
    }

    public getGenerationLimit(): number {
        return 10;
    }
}