interface IRandomNumberVisualizer {
    draw(newRandomNumber: number): void;
    getGenerationLimit(): number;
}