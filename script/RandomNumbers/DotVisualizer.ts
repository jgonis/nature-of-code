///<reference path="IRandomNumberVisualizer.ts"/>

class DotVisualizer implements IRandomNumberVisualizer {
    
    constructor(context: CanvasRenderingContext2D) {}

    public draw(newRandomNumber: number) {

    }

    public getGenerationLimit(): number {
        return 10;
    }

}