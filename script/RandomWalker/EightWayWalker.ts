///<reference path="IWalker.ts"/>
///<reference path="RandomGenerator.ts"/>
class EightWayWalker implements IWalker {

    GetNewXY(currentX: number, currentY: number, width: number, height: number): number[] {
        let rand = GetRandomInt(0, 8);
        let [newX, newY] = this.GenerateCoordinates(rand, currentX, currentY);
        while(newX < 0 || newX > width || newY < 0 || newY > height) {
            rand = GetRandomInt(0, 8);
            [newX, newY] = this.GenerateCoordinates(rand, currentX, currentY);
        }
        return [newX, newY];
    }

    private GenerateCoordinates(randResult: number, currentX: number, currentY: number): number[] {
        switch(randResult) {
            case 0:
                return [currentX, currentY - 1];
            case 1:
                return [currentX + 1, currentY - 1];
            case 2:
                return [currentX + 1, currentY];
            case 3:
                return [currentX + 1, currentY + 1];
            case 4:
                return [currentX, currentY + 1];
            case 5:
                return [currentX - 1, currentY + 1];
            case 6:
                return [currentX - 1, currentY];
            case 7:
                return [currentX - 1, currentY - 1];
            default:
                throw new RangeError("rand value was not between 0 and 3 (inclusive)");
        }
    }
}