///<reference path="IColorGenerator.ts"/>
class MonochromeColorGenerator implements IColorGenerator {

    GetStrokeStyle(): string {
        return "rgb(0, 0, 0)";
    }
}