interface IRandomWalker {
    Start(): void;
    Stop(): void;
    Reset(): void;
}