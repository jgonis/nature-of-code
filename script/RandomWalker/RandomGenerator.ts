function GetRandomInt(minimum: number, maximum: number): number {
    let min: number = Math.ceil(minimum);
    let max: number = Math.floor(maximum);
    return Math.floor(Math.random() * (max - min)) + min;
}
