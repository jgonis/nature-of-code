///<reference path="IColorGenerator.ts"/>
///<reference path="RandomGenerator.ts"/>
class ColorGenerator implements IColorGenerator {
    constructor() {
        this.m_g = GetRandomInt(0, 256);
        this.m_r = GetRandomInt(0, 256);
        this.m_b = GetRandomInt(0, 256);
    }

    GetStrokeStyle(): string {
        let [r, g, b] = this.GenerateColors(this.m_r, this.m_g, this.m_b);
        while (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255) {
            [r, g, b] = this.GenerateColors(this.m_r, this.m_g, this.m_b);
        }
        this.m_r = r;
        this.m_g = g;
        this.m_b = b;
        return "rgb(" + this.m_r + ", " + this.m_g + ", " + this.m_b + ")";
    }


    private GenerateColors(red: number, green: number, blue: number) {
        return [red + GetRandomInt(-5, 6), green + GetRandomInt(-5, 6), blue + GetRandomInt(-5, 6)];
    }

    private m_r: number;
    private m_g: number;
    private m_b: number;
}