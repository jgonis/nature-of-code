///<reference path="IRandomWalker.ts"/>
///<reference path="RandomGenerator.ts"/>
class RandomWalker implements IRandomWalker {
    constructor( canvas: HTMLCanvasElement,
                 context: Window,
                 walker: IWalker,
                 colorGenerator: IColorGenerator ) {
        this.m_canvas = canvas;
        this.m_timer = context.performance;
        this.m_context = context;
        //Start our walker in the center.
        this.m_x = Math.round(canvas.width / 2);
        this.m_y = Math.round(canvas.height / 2);
        this.m_timerId = -1;
        this.m_colorGenerator = colorGenerator;
        this.m_walker = walker;
    }

    Start() {
        this.m_timerId = this.m_context.setTimeout(()=>{this.Step();}, 0);
    }

    Step() {
        let startTime: number = this.m_timer.now();
        let currentTime: number = this.m_timer.now();
        
        let renderCtx: CanvasRenderingContext2D | null = this.m_canvas.getContext("2d");
        if(renderCtx === null)
            return;

        renderCtx.strokeStyle = this.m_colorGenerator.GetStrokeStyle();
        renderCtx.lineWidth = 0.5;
        while((currentTime - startTime) < 12) {
            renderCtx.beginPath();
            renderCtx.moveTo(this.m_x, this.m_y);
            let [x, y] = this.m_walker.GetNewXY(this.m_x, this.m_y, this.m_canvas.width, this.m_canvas.height);
            while(x < 0 || x > this.m_canvas.width || y < 0 || y > this.m_canvas.height ) {
                [x, y] = this.m_walker.GetNewXY(this.m_x, this.m_y, this.m_canvas.width, this.m_canvas.height);
            }
            renderCtx.lineTo(x, y);
            renderCtx.stroke();
            this.m_x = x;
            this.m_y = y;
            currentTime = this.m_timer.now();
        }
        this.m_timerId = this.m_context.setTimeout(()=>{this.Step();}, 0);
    }

    Stop() {
        if(this.m_timerId !== -1) {
            this.m_context.clearTimeout(this.m_timerId);
            this.m_timerId = -1;
        }
    }

    Reset() {
        let ctx: CanvasRenderingContext2D | null = this.m_canvas.getContext("2d");
        if(ctx === null)
            return;
            
        ctx.clearRect(0,0, this.m_canvas.width, this.m_canvas.height);
        this.m_x = Math.round(this.m_canvas.width / 2);
        this.m_y = Math.round(this.m_canvas.height / 2);
    }

    private m_x : number;
    private m_y : number;
    private m_canvas: HTMLCanvasElement;
    private m_timer: Performance;
    private m_context: Window;
    private m_timerId: number;
    private m_colorGenerator: IColorGenerator;
    private m_walker: IWalker;
}