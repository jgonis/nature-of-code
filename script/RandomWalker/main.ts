///<reference path="IRandomWalker.ts"/>
///<reference path="WalkerFactory.ts"/>
class AppMain {
    constructor(context: Window) {
        this.m_context = context;
        this.m_canvas = <HTMLCanvasElement>(this.m_context.document.getElementById("canvas"));
        this.m_startButton = <HTMLButtonElement>(this.m_context.document.getElementById("start"));
        this.m_stopButton = <HTMLButtonElement>(this.m_context.document.getElementById("stop"));
        this.m_resetButton = <HTMLButtonElement>(this.m_context.document.getElementById("reset"));
        this.m_walkerTypeSelector = <HTMLSelectElement>(this.m_context.document.getElementById("walkerType"));
        
        this.SetupEventHandlers();
        this.RecalcCanvas(this.m_canvas);
        this.m_walker = CreateWalker(this.m_walkerTypeSelector.value, this.m_canvas, this.m_context);
    }
    
    private RecalcCanvas(canvas: HTMLCanvasElement) {
        if(canvas !== null) { 
            if (canvas.clientWidth !== this.m_canvasWidth || canvas.clientHeight !== this.m_canvasHeight) {
                canvas.width = canvas.clientWidth;
                canvas.height = canvas.clientHeight;
                this.m_canvasWidth = canvas.clientWidth;
                this.m_canvasHeight = canvas.clientHeight;

            }
        }
    }

    private SetupEventHandlers() {
        this.m_context.onresize = (event: UIEvent) => {
            this.m_context.requestAnimationFrame(() => {
                this.RecalcCanvas(this.m_canvas);
            })
        };
        this.m_startButton.addEventListener("click", (event: MouseEvent) => { this.StartClicked(event)});
        this.m_stopButton.addEventListener("click", (event: MouseEvent) => { this.StopClicked(event)});
        this.m_resetButton.addEventListener("click", (event: MouseEvent) => { this.ResetClicked(event)});
        this.m_walkerTypeSelector.addEventListener("change", (event: Event) => { this.WalkerTypeChanged(event)});
    }

    private StartClicked(event: MouseEvent) {
        console.log("start clicked!");
        this.m_startButton.disabled = true;
        this.m_stopButton.disabled = false;
        this.m_resetButton.disabled = true;
        this.m_walkerTypeSelector.disabled = true;
        this.m_walker.Start();
    }

    private StopClicked(event: MouseEvent) {
        this.m_walker.Stop();
        console.log("stop clicked!");
        this.m_startButton.disabled = false;
        this.m_resetButton.disabled = false;
        this.m_stopButton.disabled = true;
        this.m_walkerTypeSelector.disabled = false;
    }

    private ResetClicked(event: MouseEvent) {
        console.log("reset clicked!");
        this.m_walker.Reset();
    }

    private WalkerTypeChanged(event: Event) {
        console.log("walker type changed!");
        console.log(this.m_walkerTypeSelector.value);
        this.m_walker.Reset();
        this.m_walker = CreateWalker(this.m_walkerTypeSelector.value, this.m_canvas, this.m_context);
    }

    private m_canvasWidth: number = -1;
    private m_canvasHeight: number = -1;
    private m_walker : IRandomWalker;

    private m_canvas: HTMLCanvasElement;
    private m_context: Window;
    private m_startButton : HTMLButtonElement;
    private m_stopButton : HTMLButtonElement;
    private m_resetButton : HTMLButtonElement;
    private m_walkerTypeSelector : HTMLSelectElement;
}

((context: Window) => {
    context.onload = () => {
        myInit(context);
    }

    let app: AppMain;

    function myInit(context: Window) {
        app = new AppMain(context);
    }
})(window);