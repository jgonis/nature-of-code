///<reference path="IRandomWalker.ts"/>
///<reference path="RandomWalker.ts"/>
///<reference path="FourWayWalker.ts"/>
///<reference path="EightWayWalker.ts"/>
///<reference path="MonochromeColorGenerator.ts"/>
///<reference path="ColorGenerator.ts"/>
function CreateWalker(walkerType : string, canvas: HTMLCanvasElement, context: Window) : IRandomWalker {
    let walker: IRandomWalker;

    switch(walkerType) {
        case "BandW4Way":
            walker = new RandomWalker(canvas, context, new FourWayWalker(), new MonochromeColorGenerator() );
        break;
        case "BandW8Way":
            walker = new RandomWalker(canvas, context, new EightWayWalker(), new MonochromeColorGenerator());
        break;
        case "Color4Way":
            walker = new RandomWalker(canvas, context, new FourWayWalker(), new ColorGenerator());
        break;
        case "Color8Way":
            walker = new RandomWalker(canvas, context, new EightWayWalker(), new ColorGenerator());
        break;
        default:
            throw new RangeError("Unknown Walker Type");
    }        

    return walker;
}