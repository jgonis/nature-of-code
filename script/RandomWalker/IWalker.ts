interface IWalker {
    GetNewXY(currentX: number, currentY: number, width: number, height: number): number[];
}