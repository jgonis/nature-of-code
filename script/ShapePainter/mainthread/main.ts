class AppMain {
    constructor(context: Window) {
        this.m_context = context;
        this.m_canvas = <HTMLCanvasElement>(this.m_context.document.getElementById("canvas"));
        this.m_fileInput = <HTMLInputElement>(this.m_context.document.getElementById("imageFile"))
        this.m_stopButton = <HTMLButtonElement>(this.m_context.document.getElementById("stop"));
        this.m_resetButton = <HTMLButtonElement>(this.m_context.document.getElementById("reset"));
        
        this.SetupEventHandlers();
        this.RecalcCanvas(this.m_canvas);
    }
    
    private RecalcCanvas(canvas: HTMLCanvasElement) {
        if(canvas !== null) { 
            if (canvas.clientWidth !== this.m_canvasWidth || canvas.clientHeight !== this.m_canvasHeight) {
                canvas.width = canvas.clientWidth;
                canvas.height = canvas.clientHeight;
                this.m_canvasWidth = canvas.clientWidth;
                this.m_canvasHeight = canvas.clientHeight;

            }
        }
    }

    private SetupEventHandlers() {
        this.m_context.onresize = (event: UIEvent) => {
            this.m_context.requestAnimationFrame(() => {
                this.RecalcCanvas(this.m_canvas);
            })
        };
        this.m_stopButton.addEventListener("click", (event: MouseEvent) => { this.StopClicked(event)});
        this.m_resetButton.addEventListener("click", (event: MouseEvent) => { this.ResetClicked(event)});
        this.m_fileInput.addEventListener("change", (event: Event) => {this.FileSelected(event)});
    }

    private FileSelected(event) {
        console.log("file selected");
        if(this.m_fileInput !== null && this.m_fileInput.files !== null) {
            console.log(this.m_fileInput.files.length);
        }
    }

    private StartClicked(event: MouseEvent) {
        console.log("start clicked!");
        this.m_stopButton.disabled = false;
        this.m_resetButton.disabled = true;
    }

    private StopClicked(event: MouseEvent) {
        console.log("stop clicked!");
        this.m_resetButton.disabled = false;
        this.m_stopButton.disabled = true;
    }

    private ResetClicked(event: MouseEvent) {
        console.log("reset clicked!");
    }

    private m_canvasWidth: number = -1;
    private m_canvasHeight: number = -1;

    private m_canvas: HTMLCanvasElement;
    private m_context: Window;
    private m_fileInput: HTMLInputElement;
    private m_stopButton: HTMLButtonElement;
    private m_resetButton: HTMLButtonElement;
}

((context: Window) => {
    context.onload = () => {
        myInit(context);
    }

    let app: AppMain;

    function myInit(context: Window) {
        app = new AppMain(context);
    }
})(window);