class AppMain {
    constructor(context: Window) {
        this.m_context = context;
        this.m_canvas = <HTMLCanvasElement>(this.m_context.document.getElementById("canvas"));
        this.m_startButton = <HTMLButtonElement>(this.m_context.document.getElementById("start"));
        this.m_stopButton = <HTMLButtonElement>(this.m_context.document.getElementById("stop"));
        this.m_resetButton = <HTMLButtonElement>(this.m_context.document.getElementById("reset"));
        
        this.SetupEventHandlers();
        this.InitializeWorkers();
    }

    private SetupEventHandlers() {
        console.log("setting up events");
        this.m_startButton.addEventListener("click", (event: MouseEvent) => { this.StartClicked(event);});
        this.m_stopButton.addEventListener("click", (event: MouseEvent) => { this.StopClicked(event);});
        this.m_resetButton.addEventListener("click", (event: MouseEvent) => {this.ResetClicked(event);});
    }

    private InitializeWorkers() { 
        this.m_workers = [];
        for(let i = 0; i < 4; i++) {
            this.m_workers.push( new Worker("build/RayTracerWorker.js") ); 
        }
    } 

    private StartClicked(event: MouseEvent) {
        console.log("start clicked!");
        //this.m_stopButton.disabled = false;
        //this.m_resetButton.disabled = true;
        let ctx = this.m_canvas.getContext("2d");
        if( ctx !== null)
            this.WriteImage(ctx);
    }

    private WriteImage(ctx: CanvasRenderingContext2D) {
        let start: number = this.m_context.performance.now();
        //let rows: number = Math.floor(ctx.canvas.height / this.m_workers.length);
        for(let counter = 0; counter < this.m_workers.length; counter++) {
            if(counter === (this.m_workers.length - 1)) {
                this.m_workers[counter].addEventListener("message", (ev: MessageEvent) => {
                    console.log("received message");
                    this.m_workers[counter].removeEventListener("message");
                });
                this.m_workers[counter].postMessage("jeff");
            } else {
                
            }
        }
        //divide rows by number of workers. 
        //floor the result.  
        //if counter === # of workers - 1
            //go from rows * counter to end
        //else
            //go from counter * rows to ((counter * rows + rows) - 1)
        
        let canvasData: ImageData = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
        let numberOfPixels = ctx.canvas.width * ctx.canvas.height;
        let bytesPerPixel = canvasData.data.length / numberOfPixels;
        for(let i = 0; i < numberOfPixels; i++) {
            let currentIndex = i * bytesPerPixel;
            canvasData.data[currentIndex] = 128;
            canvasData.data[currentIndex + 1]=128;
            canvasData.data[currentIndex + 2]=128;
            canvasData.data[currentIndex + 3]=255;
        }
        ctx.putImageData(canvasData, 0, 0);
        let end = this.m_context.performance.now();
        console.log("elapsed time: " + (end - start));
    }

    private StopClicked(event: MouseEvent) {
        console.log("stop clicked!");
        this.m_resetButton.disabled = false;
        this.m_stopButton.disabled = true;
    }

    private ResetClicked(event: MouseEvent) {
        console.log("reset clicked!");
    }

    private m_canvas: HTMLCanvasElement;
    private m_context: Window;
    private m_startButton: HTMLButtonElement;
    private m_stopButton: HTMLButtonElement;
    private m_resetButton: HTMLButtonElement;
    private m_workers: Worker[];
}

((context: Window) => {
    context.onload = () => {
        myInit(context);
    }

    let app: AppMain;

    function myInit(context: Window) {
        app = new AppMain(context);
    }
})(window);