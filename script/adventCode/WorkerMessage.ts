/**
 * Created by jeffg on 12/13/2016.
 */

class WorkerMessage {
    constructor(problem: string, solution: string) {
        this.problem = problem;
        this.solution = solution;
    }

    readonly problem: string;
    readonly solution: string;
}