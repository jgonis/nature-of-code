///<reference path="../WorkerMessage.ts"/>
///<reference path="../MainThreadMessage.ts"/>

class AppMain {
    constructor(context: Window) {
        this.m_context = context;

        this.m_startButton = <HTMLButtonElement>(this.m_context.document.getElementById("start"));
        this.m_stopButton = <HTMLButtonElement>(this.m_context.document.getElementById("stop"));
        this.m_problemSelector = <HTMLSelectElement>(this.m_context.document.getElementById("AdventChallenge"));

        this.m_worker = new Worker("build/AdventWorker.js");
        this.SetupEventHandlers();
        this.m_worker.postMessage(new MainThreadMessage(this.m_problemSelector.value, 'construct'));
    }

    private SetupEventHandlers() {
        this.m_startButton.addEventListener("click",
            (event: MouseEvent) => {
                this.StartClicked(event)
            });
        this.m_stopButton.addEventListener("click",
            (event: MouseEvent) => {
                this.StopClicked(event)
            });

        this.m_problemSelector.addEventListener("change", (event: Event) => {
            this.ProblemChanged(event)
        });

        this.m_worker.addEventListener('message', (ev: MessageEvent) => {
            this.WorkerMessageReceived(ev);
        });
    }


    private WorkerMessageReceived(message: MessageEvent) {
        this.m_startButton.disabled = false;
        this.m_stopButton.disabled = true;
        let result: WorkerMessage = message.data;
        console.log(result.solution);
    }

    private StartClicked(event: MouseEvent) {
        this.m_startButton.disabled = true;
        this.m_stopButton.disabled = false;
        this.m_worker.postMessage(new MainThreadMessage(this.m_problemSelector.value, 'start'));
    }

    private StopClicked(event: MouseEvent) {
        this.m_startButton.disabled = false;
        this.m_stopButton.disabled = true;
    }

    private ProblemChanged(event: Event) {
        this.m_worker.postMessage(new MainThreadMessage(this.m_problemSelector.value, 'construct'));
    }

    private m_context: Window;
    private m_startButton: HTMLButtonElement;
    private m_stopButton: HTMLButtonElement;
    private m_problemSelector: HTMLSelectElement;
    private m_worker: Worker;
}

((context: Window) => {
    context.onload = () => {
        myInit(context);
    }

    let app: AppMain;

    function myInit(context: Window) {
        app = new AppMain(context);
    }
})(window);