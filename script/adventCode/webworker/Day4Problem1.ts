/**
 * Created by jeffg on 12/23/2016.
 */

declare interface Response {
    text(): Promise<string>;
}
class Day4Problem1 implements IProblemSolver {

    constructor(context: WorkerGlobalScope) {
        this.m_context = context;
    }

    solve(): Promise<string> {
        return this.m_context.fetch('../day4input.txt').then((response: Response) => {
            return response.text();
        }).then((input: string) => {
            let roomCodes: Array<Array<string>> = this.ProcessInput(input);
            let sectorIdSum = this.ValidateRoomNames(roomCodes);
            return Promise.resolve(sectorIdSum.toString());
        });
    }

    private ProcessInput(input: string): Array<Array<string>> {
        let lines = input.split('\n');
        let processedLines: Array<Array<string>> = new Array();
        for (const line of lines) {
            let temp = line.split('-');
            let sectorAndChecksum = temp[temp.length - 1];
            let sectorCode: string = (sectorAndChecksum.split('['))[0];
            let rankingString: string = (sectorAndChecksum.split('['))[1];
            rankingString = rankingString.substring(0, rankingString.length - 1);
            temp.pop();
            processedLines.push([temp.join(''), sectorCode, rankingString]);
        }
        return processedLines;
    }

    private ValidateRoomNames(roomInfo: Array<Array<string>>): number {
        let sectorSum = 0;
        for (const room of roomInfo) {
            sectorSum = sectorSum + this.ValidateRoom(room);
        }
        return sectorSum;
    }

    private ValidateRoom(room: Array<string>): number {
        let roomName = room[0];
        let letterFreqMap: Map<string, number> = new Map();
        for (const letter of roomName) {
            if (letterFreqMap.has(letter)) {
                letterFreqMap.set(letter, letterFreqMap.get(letter) + 1);
            } else {
                letterFreqMap.set(letter, 1);
            }
        }
        let freqArray = Array.from(letterFreqMap.entries());
        freqArray.sort((a, b) => {
            let freqA = a[1];
            let freqB = b[1];
            if (freqA > freqB)
                return -1;
            else if (freqA < freqB)
                return 1;
            else {
                if (a[0] < b[0])
                    return -1;
                else if (a[0] > b[0])
                    return 1;
                else
                    return 0;
            }
        });
        let freqString = '';
        for (let i = 0; i < 5; i++) {
            freqString = freqString + freqArray[i][0];
        }

        if (freqString === room[2]) {
            let sectorCodeNumber = Number.parseInt(room[1]);
            this.DisplayDecryptedName(roomName, sectorCodeNumber);
            return sectorCodeNumber;
        } else {
            return 0;
        }
    }

    protected DisplayDecryptedName(roomName: string, roomCode: number) {

    }

    private m_context: WorkerGlobalScope;
}