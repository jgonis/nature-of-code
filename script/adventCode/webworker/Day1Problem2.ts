/**
 * Created by jeffg on 12/12/2016.
 */
///<reference path="IProblemSolver.ts"/>
///<reference path="../../libs/Geometry/Point.ts"/>
///<reference path="../../libs/Geometry/Vector.ts"/>

class Day1Problem2 implements IProblemSolver {
    public solve(): Promise<string> {
        const inputString: String = 'R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5';
        let currentPosition: Point = new Point(0, 0);
        let currentDirection = new Vector(0, 1);
        let inputs: Array<string> = inputString.split(',');
        inputs = inputs.map((elem: string) => {
            return elem.trim();
        });

        let previousLocations: Array<Point> = [];
        previousLocations.push(currentPosition.copy());

        for (const input of inputs) {
            if (input.charAt(0).toLocaleLowerCase() === 'l') {
                currentDirection = currentDirection.rotate(Math.PI / 2);
            }
            else {
                currentDirection = currentDirection.rotate(-(Math.PI / 2));
            }
            const scaleFactor: number = Number.parseInt(input.slice(1));
            for (let i = 0; i < scaleFactor; i++) {
                currentPosition = currentPosition.add(currentDirection);
                if (previousLocations.findIndex((point: Point) => {
                        return point.equals(currentPosition);
                    }) !== -1) {
                    return Promise.resolve( (Math.abs(currentPosition.x())
                    + (Math.abs(currentPosition.y()))).toString());

                }
                previousLocations.push(currentPosition.copy());
            }
        }
        return Promise.resolve('');
    }
}