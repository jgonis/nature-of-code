/**
 * Created by jeffg on 12/11/2016.
 */

///<reference path="IProblemSolver.ts"/>
///<reference path="../../libs/Geometry/Point.ts"/>
///<reference path="../../libs/Geometry/Vector.ts"/>

class Day1Problem1 implements IProblemSolver {
    public solve(): Promise<string> {
        const inputString: String = 'R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5';
        let currentPosition: Point = new Point(0, 0);
        let currentDirection = new Vector(0, 1);
        let inputs: Array<string> = inputString.split(',');
        inputs = inputs.map((elem: string) => {
            return elem.trim();
        });

        for (const input of inputs) {
            if (input.charAt(0).toLocaleLowerCase() === 'l') {
                currentDirection = currentDirection.rotate(Math.PI / 2);
            }
            else {
                currentDirection = currentDirection.rotate(-(Math.PI / 2));
            }
            const scaleFactor: number = Number.parseInt(input.slice(1));
            currentPosition = currentPosition.add(currentDirection.scale(scaleFactor));

        }
        // We make a new promise: we promise a numeric count of this promise, starting from 1 (after waiting 3s)
        return Promise.resolve((Math.abs(currentPosition.x()) + (Math.abs(currentPosition.y()))).toString());
    }
}