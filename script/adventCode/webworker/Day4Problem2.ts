/**
 * Created by jeffg on 12/23/2016.
 */

class Day4Problem2 extends Day4Problem1 {

    constructor(context: WorkerGlobalScope) {
        super(context);
    }

    solve(): Promise<string> {
        return super.solve();
    }

    protected DisplayDecryptedName(roomName: string, roomCode: number) {
        let startingCharCode = 'a'.charCodeAt(0);
        let decryptedName: string = '';
        let offset: number = roomCode % 26;
        for(let char of roomName) {
            let charNum: number = char.charCodeAt(0) - startingCharCode;
            let newNum = (charNum + offset) % 26;
            decryptedName = decryptedName + String.fromCharCode(newNum + startingCharCode);
        }

        console.log(decryptedName + ' ' + roomCode);
    }
}
