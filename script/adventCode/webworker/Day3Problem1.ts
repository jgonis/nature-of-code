/**
 * Created by jeffg on 12/18/2016.
 */


class Day3Problem1 implements IProblemSolver {

    constructor(context: WorkerGlobalScope ) {
        this.m_context = context;
    }

    solve(): Promise<string> {
        return this.m_context.fetch('../day3input.txt').then((response: Response)=> {
            return response.text();
        }).then((input: string) => {
            let triangleDefinitions: Array<Array<string>> = this.ParseInput(input);
            let count = 0;
            for(const triangle of triangleDefinitions) {
                const sideTotal: number = Number.parseInt(triangle[0]) + Number.parseInt(triangle[1]);
                const thirdSide: number = Number.parseInt(triangle[2]);
                if(sideTotal > thirdSide ) {
                    count = count + 1;
                }
            }
            return Promise.resolve(count.toLocaleString());
        });
    }

    private ParseInput(input: string) {
        let triangleDefinitions: Array<string> = input.split('\n');
        let parsedDefinitions: Array<Array<string>> = triangleDefinitions.map((input: string) => {
            let trimmed: string = input.trim();
            let defs: Array<string> = trimmed.split(' ');
            let noBlanks: Array<string> = defs.filter((input: string) => { return input.length != 0;});
            let sorted: Array<string> = noBlanks.sort((a: string, b: string) => {
                let num1 = Number.parseInt(a);
                let num2 = Number.parseInt(b);
                if(num1 < num2) return -1;
                else if(num1 === num2) return 0;
                else return 1;
            });
            return sorted;
        });
        return parsedDefinitions;
    }

    private m_context: WorkerGlobalScope;
}