/**
 * Created by jeffg on 12/13/2016.
 */
///<reference path="IProblemSolver.ts"/>
///<reference path="ProblemSolverFactory.ts"/>
///<reference path="../WorkerMessage.ts"/>
///<reference path="../MainThreadMessage.ts"/>
((context: WorkerGlobalScope) => {
    let solver: IProblemSolver;
    let problemName: string;

    context.onmessage = (event: MessageEvent) => {
        let mainThreadMessage: MainThreadMessage = event.data;

        switch (mainThreadMessage.command) {
            case 'construct':
                problemName = mainThreadMessage.currentProblem;
                solver = CreateProblemSolver(problemName, context);
                break;
            case 'start': {
                let result: Promise<string> = solver.solve();
                result.then((resultText: string) => {
                    let resultObj: WorkerMessage = new WorkerMessage(problemName, resultText);
                    postMessage(resultObj);
                });
            }
        }

    }
})(self);


