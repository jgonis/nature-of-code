/**
 * Created by jeffg on 12/18/2016.
 */

class Day3Problem2 implements IProblemSolver {

    constructor(context: WorkerGlobalScope) {
        this.m_context = context;
    }

    solve(): Promise<string> {
        return this.m_context.fetch('../day3input.txt').then((response: Response) => {
            return response.text();
        }).then((input: string) => {
            let triangleDefinitions: Array<Array<string>> = this.ParseInput(input);
            let count = 0;
            for (let i = 0; i < triangleDefinitions.length; i = i + 3) {
                for(let j = 0; j < 3; j++) {
                    if (this.AnalyzeTriangleDefinition(triangleDefinitions, i, j) === true) {
                        count = count + 1;
                    }
                }
            }
            return Promise.resolve(count.toLocaleString());
        });
    }

    private AnalyzeTriangleDefinition(triangleDefinitions: Array<Array<string>>,
                                      row: number,
                                      column: number): boolean {
        let triangleSides = [triangleDefinitions[row][column],
            triangleDefinitions[row + 1][column],
            triangleDefinitions[row + 2][column]];

        let sortedSides: Array<string> = triangleSides.sort((a: string, b: string) => {
            let num1 = Number.parseInt(a);
            let num2 = Number.parseInt(b);
            if(num1 < num2) return -1;
            else if(num1 === num2) return 0;
            else return 1;
        });

        let sideTotal: number = Number.parseInt(sortedSides[0]) + Number.parseInt(sortedSides[1]);
        let thirdSide: number = Number.parseInt(sortedSides[2]);

        if (sideTotal > thirdSide) {
            return true;
        }
        return false;
    }

    private ParseInput(input: string) {
        let triangleDefinitions: Array<string> = input.split('\n');

        let parsedDefinitions: Array<Array<string>> = triangleDefinitions.map((input: string) => {
            let trimmed: string = input.trim();
            let defs: Array<string> = trimmed.split(' ');
            let noBlanks: Array<string> = defs.filter((input: string) => { return input.length != 0;});
            return noBlanks;
        });
        return parsedDefinitions;
    }


    private m_context: WorkerGlobalScope;
}