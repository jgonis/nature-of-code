/**
 * Created by jeffg on 12/11/2016.
 */
interface IProblemSolver {
    solve(): Promise<string>;
}

interface IPromiseSolver {
    solve(): Promise<string>;
}