/**
 * Created by jeffg on 12/11/2016.
 */
///<reference path="IProblemSolver.ts"/>
///<reference path="Day1Problem1.ts"/>
///<reference path="Day1Problem2.ts"/>
///<reference path="Day2Problem1.ts"/>
///<reference path="Day3Problem1.ts"/>
///<reference path="Day3Problem2.ts"/>
///<reference path="Day4Problem1.ts"/>
///<reference path="Day4Problem2.ts"/>

function CreateProblemSolver(problem: string, context: WorkerGlobalScope): IProblemSolver {
    switch (problem) {
        case 'D1P1':
            return new Day1Problem1();
        case 'D1P2':
            return new Day1Problem2();
        case 'D2P1':
            return new Day2Problem1();
        case 'D2P2':
            return new Day2Problem2();
        case 'D3P1':
            return new Day3Problem1(context);
        case 'D3P2':
            return new Day3Problem2(context);
        case 'D4P1':
            return new Day4Problem1(context);
        case 'D4P2':
            return new Day4Problem2(context);
        default:
            throw Error;
    }

}