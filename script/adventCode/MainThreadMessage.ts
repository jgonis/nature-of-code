/**
 * Created by jeffg on 12/13/2016.
 */

class MainThreadMessage {
    constructor(currentProblem: string, command: string) {
        this.currentProblem = currentProblem;
        this.command = command;
    }

    readonly currentProblem: string;
    readonly command: string;
}