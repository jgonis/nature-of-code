/**
 * Created by jeffg on 12/13/2016.
 */
var MainThreadMessage = (function () {
    function MainThreadMessage(currentProblem, command) {
        this.currentProblem = currentProblem;
        this.command = command;
    }
    return MainThreadMessage;
}());
