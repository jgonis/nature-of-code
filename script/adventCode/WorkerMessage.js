/**
 * Created by jeffg on 12/13/2016.
 */
var WorkerMessage = (function () {
    function WorkerMessage(problem, solution) {
        this.problem = problem;
        this.solution = solution;
    }
    return WorkerMessage;
}());
