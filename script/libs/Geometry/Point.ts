/**
 * Created by jeffg_000 on 12/5/2016.
 */
///<reference path="Vector.ts"/>
class Point {
    constructor(x: number, y: number) {
        this.m_x = x;
        this.m_y = y;
    }

    add(vec: Vector): Point {
        return new Point( this.m_x + vec.xComponent(), this.m_y + vec.yComponent());
    }

    x(): number {
        return this.m_x;
    }

    y(): number {
        return this.m_y;
    }

    subtract(otherPoint: Point): Point {
        return new Point(this.m_x - otherPoint.x(), this.m_y - otherPoint.y());
    }

    copy(): Point {
        return new Point(this.m_x, this.m_y);
    }

    equals(otherPoint: Point): boolean {
        return ((this.m_x === otherPoint.x()) && (this.m_y === otherPoint.y()));
    }

    toString(): string {
        return this.x().toLocaleString() + ', ' + this.y().toLocaleString();
    }
    
    private m_x: number;
    private m_y: number;
}