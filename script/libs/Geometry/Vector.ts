/**
 * Created by jeffg_000 on 12/5/2016.
 */

class Vector {
    constructor(xComponent: number, yComponent: number) {
        this.m_xComponent = xComponent;
        this.m_yComponent = yComponent;
    }

    xComponent(): number {
        return this.m_xComponent;
    }

    yComponent(): number {
        return this.m_yComponent;
    }

    rotate(angleInRadians: number): Vector {
        const newX: number = (this.m_xComponent * Math.cos(angleInRadians))
            - (this.m_yComponent * Math.sin(angleInRadians));
        const newY: number = (this.m_xComponent * Math.sin(angleInRadians))
            + (this.m_yComponent * Math.cos(angleInRadians));
        return new Vector(Number.parseFloat(newX.toFixed(10)), Number.parseFloat(newY.toFixed(10)));
    }

    scale(scaleFactor: number): Vector {
        return new Vector(this.m_xComponent * scaleFactor, this.m_yComponent * scaleFactor);
    }
    private m_xComponent: number;
    private m_yComponent: number;
}